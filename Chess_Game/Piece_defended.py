# analizeaza daca o piesa este aparata atunci cand se pune problema
# ca regele sa mute si sa ia piesa aadevrsara


import Bishop_behavior as bib
import Knight_behavior as knb
import WPawn_behavior as wpb
import BPawn_behavior as bpb
import Queen_behavior as qub
import Rock_behavior as rob
import King_behavior_bkp as kib


#  lista cu literele de pe tabla de sah
list_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

#  lista cu numerele de pe tabla de sah
list_numbers = [1, 2, 3, 4, 5, 6, 7, 8]

#  dictionar in care se atribuie un set de valori intregi pozitilor de pe tabla de sah,
#  A este considerata 1 pe axa x, H este considerata 8 pe axa x, iar y ia valorile intre 1 si 8
#  pentru a studia matematic comportamentul fiecarei piese, a verifica mutarea posibila si a muta.
chess_pos_dictionary = {
    'A1': 11, 'B1': 21, 'C1': 31, 'D1': 41, 'E1': 51, 'F1': 61, 'G1': 71, 'H1': 81,
    'A2': 12, 'B2': 22, 'C2': 32, 'D2': 42, 'E2': 52, 'F2': 62, 'G2': 72, 'H2': 82,
    'A3': 13, 'B3': 23, 'C3': 33, 'D3': 43, 'E3': 53, 'F3': 63, 'G3': 73, 'H3': 83,
    'A4': 14, 'B4': 24, 'C4': 34, 'D4': 44, 'E4': 54, 'F4': 64, 'G4': 74, 'H4': 84,
    'A5': 15, 'B5': 25, 'C5': 35, 'D5': 45, 'E5': 55, 'F5': 65, 'G5': 75, 'H5': 85,
    'A6': 16, 'B6': 26, 'C6': 36, 'D6': 46, 'E6': 56, 'F6': 66, 'G6': 76, 'H6': 86,
    'A7': 17, 'B7': 27, 'C7': 37, 'D7': 47, 'E7': 57, 'F7': 67, 'G7': 77, 'H7': 87,
    'A8': 18, 'B8': 28, 'C8': 38, 'D8': 48, 'E8': 58, 'F8': 68, 'G8': 78, 'H8': 88,
}

#  dictionar care reprezinta tabla de sah initiala,
#  nu va suferi modificari in timpul jocului.
chess_table_init = {
    'A1': 'W_Rock1', 'B1': 'W_Knight1', 'C1': 'W_Bishop1', 'D1': 'W_Queen', 'E1': 'W_King', 'F1': 'W_Bishop2',
    'G1': 'W_Knight2', 'H1': 'W_Rock2',
    'A2': 'W_Pawn1', 'B2': 'W_Pawn2', 'C2': 'W_Pawn3', 'D2': 'W_Pawn4', 'E2': 'W_Pawn5', 'F2': 'W_Pawn6',
    'G2': 'W_Pawn7', 'H2': 'W_Pawn8',
    'A3': ' ', 'B3': ' ', 'C3': ' ', 'D3': ' ', 'E3': ' ', 'F3': ' ', 'G3': ' ', 'H3': ' ',
    'A4': ' ', 'B4': ' ', 'C4': ' ', 'D4': ' ', 'E4': ' ', 'F4': ' ', 'G4': ' ', 'H4': ' ',
    'A5': ' ', 'B5': ' ', 'C5': ' ', 'D5': ' ', 'E5': ' ', 'F5': ' ', 'G5': ' ', 'H5': ' ',
    'A6': ' ', 'B6': ' ', 'C6': ' ', 'D6': ' ', 'E6': ' ', 'F6': ' ', 'G6': ' ', 'H6': ' ',
    'A7': 'B_Pawn8', 'B7': 'B_Pawn7', 'C7': 'B_Pawn6', 'D7': 'B_Pawn5', 'E7': 'B_Pawn4', 'F7': 'B_Pawn3',
    'G7': 'B_Pawn2', 'H7': 'B_Pawn1',
    'A8': 'B_Rock2', 'B8': 'B_Knight2', 'C8': 'B_Bishop2', 'D8': 'B_Queen', 'E8': 'B_King', 'F8': 'B_Bishop1',
    'G8': 'B_Knight1', 'H8': 'B_Rock1',
}

# chess_table_current1 = {}
# for j in chess_table_init.keys():
#     a = chess_table_init[j]
#     chess_table_current1.update({j: a})


def piece_defended(pos_stop, chess_table_current, number_move):
    # verifica daca piesa aflata pe pozitia de stop este aparata sau nu

    x_stop = chess_pos_dictionary[pos_stop] // 10
    y_stop = chess_pos_dictionary[pos_stop] % 10

    if number_move % 2 != 0:
        color_player = 'W_'
    else:
        color_player = 'B_'

    if ' ' in chess_table_current[pos_stop]:
        return False
    else:
        remaining_pieces = {}
        piece_name = chess_table_current[pos_stop]

        for i in chess_table_current.keys():
            if (color_player not in chess_table_current[i]) and (
                    piece_name not in chess_table_current[i]):
                value_update = chess_table_current[i]
                remaining_pieces.update({i: value_update})

        defended = 0

        for i in remaining_pieces.keys():
            if 'Rock' in remaining_pieces[i]:
                trace_list = rob.rock_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        rob.rock_piece_defender(pos_stop, chess_table_current, number_move):
                    defended += 1
            if 'Knight' in remaining_pieces[i]:
                trace_list = knb.knight_behavior(i, pos_stop)
                if (x_stop * 10 + y_stop) in trace_list and \
                        knb.knight_piece_defender(pos_stop, chess_table_current, number_move):
                    defended += 1
            if 'Bishop' in remaining_pieces[i]:
                trace_list = bib.bishop_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        bib.bishop_piece_defender(pos_stop, chess_table_current, number_move):
                    defended += 1
            if 'Queen' in remaining_pieces[i]:
                trace_list = qub.queen_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        qub.queen_piece_defender(pos_stop, chess_table_current, number_move):
                    defended += 1
            if 'W_Pawn' in remaining_pieces[i]:
                trace_list = wpb.wpawn_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        wpb.wpawn_piece_defender(pos_stop, chess_table_current):
                    defended += 1
            if 'B_Pawn' in remaining_pieces[i]:
                trace_list = bpb.bpawn_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        bpb.bpawn_piece_defender(pos_stop, chess_table_current):
                    defended += 1
            elif 'King' in remaining_pieces[i]:
                trace_list = kib.king_behavior(i, pos_stop)
                if ((x_stop * 10 + y_stop) in trace_list) and \
                        kib.king_piece_attacker(pos_stop, trace_list, chess_table_current):
                    defended += 1

    if defended > 0:
        return True
    else:
        return False
    # testat ok fara rege
