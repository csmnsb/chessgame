# analizeaza comportamentul calului


#  lista cu literele de pe tabla de sah
list_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

#  lista cu numerele de pe tabla de sah
list_numbers = [1, 2, 3, 4, 5, 6, 7, 8]

#  dictionar in care se atribuie un set de valori intregi pozitilor de pe tabla de sah,
#  A este considerata 1 pe axa x, H este considerata 8 pe axa x, iar y ia valorile intre 1 si 8
#  pentru a studia matematic comportamentul fiecarei piese, a verifica mutarea posibila si a muta.
chess_pos_dictionary = {
    'A1': 11, 'B1': 21, 'C1': 31, 'D1': 41, 'E1': 51, 'F1': 61, 'G1': 71, 'H1': 81,
    'A2': 12, 'B2': 22, 'C2': 32, 'D2': 42, 'E2': 52, 'F2': 62, 'G2': 72, 'H2': 82,
    'A3': 13, 'B3': 23, 'C3': 33, 'D3': 43, 'E3': 53, 'F3': 63, 'G3': 73, 'H3': 83,
    'A4': 14, 'B4': 24, 'C4': 34, 'D4': 44, 'E4': 54, 'F4': 64, 'G4': 74, 'H4': 84,
    'A5': 15, 'B5': 25, 'C5': 35, 'D5': 45, 'E5': 55, 'F5': 65, 'G5': 75, 'H5': 85,
    'A6': 16, 'B6': 26, 'C6': 36, 'D6': 46, 'E6': 56, 'F6': 66, 'G6': 76, 'H6': 86,
    'A7': 17, 'B7': 27, 'C7': 37, 'D7': 47, 'E7': 57, 'F7': 67, 'G7': 77, 'H7': 87,
    'A8': 18, 'B8': 28, 'C8': 38, 'D8': 48, 'E8': 58, 'F8': 68, 'G8': 78, 'H8': 88,
}

#  dictionar care reprezinta tabla de sah initiala,
#  nu va suferi modificari in timpul jocului.
chess_table_init = {
    'A1': 'W_Rock1', 'B1': 'W_Knight1', 'C1': 'W_Bishop1', 'D1': 'W_Queen', 'E1': 'W_King', 'F1': 'W_Bishop2',
    'G1': 'W_Knight2', 'H1': 'W_Rock2',
    'A2': 'W_Pawn1', 'B2': 'W_Pawn2', 'C2': 'W_Pawn3', 'D2': 'W_Pawn4', 'E2': 'W_Pawn5', 'F2': 'W_Pawn6',
    'G2': 'W_Pawn7', 'H2': 'W_Pawn8',
    'A3': ' ', 'B3': ' ', 'C3': ' ', 'D3': ' ', 'E3': ' ', 'F3': ' ', 'G3': ' ', 'H3': ' ',
    'A4': ' ', 'B4': ' ', 'C4': ' ', 'D4': ' ', 'E4': ' ', 'F4': ' ', 'G4': ' ', 'H4': ' ',
    'A5': ' ', 'B5': ' ', 'C5': ' ', 'D5': ' ', 'E5': ' ', 'F5': ' ', 'G5': ' ', 'H5': ' ',
    'A6': ' ', 'B6': ' ', 'C6': ' ', 'D6': ' ', 'E6': ' ', 'F6': ' ', 'G6': ' ', 'H6': ' ',
    'A7': 'B_Pawn8', 'B7': 'B_Pawn7', 'C7': 'B_Pawn6', 'D7': 'B_Pawn5', 'E7': 'B_Pawn4', 'F7': 'B_Pawn3',
    'G7': 'B_Pawn2', 'H7': 'B_Pawn1',
    'A8': 'B_Rock2', 'B8': 'B_Knight2', 'C8': 'B_Bishop2', 'D8': 'B_Queen', 'E8': 'B_King', 'F8': 'B_Bishop1',
    'G8': 'B_Knight1', 'H8': 'B_Rock1',
}

#
# chess_table_current1 = {}
# for j in chess_table_init.keys():
#     a = chess_table_init[j]
#     chess_table_current1.update({j: a})


def knight_behavior(pos_start, pos_stop):
    # analizeaza mutarea calului de la pozitia de start
    x_start = chess_pos_dictionary[pos_start] // 10
    y_start = chess_pos_dictionary[pos_start] % 10
    x_stop = chess_pos_dictionary[pos_stop] // 10
    y_stop = chess_pos_dictionary[pos_stop] % 10

    pos_move = []

    if (x_stop == x_start + 1) and (y_stop == y_start + 2):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start + 1) and (y_stop == y_start - 2):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start + 2) and (y_stop == y_start + 1):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start + 2) and (y_stop == y_start - 1):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start - 1) and (y_stop == y_start + 2):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start - 1) and (y_stop == y_start - 2):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start - 2) and (y_stop == y_start + 1):
        pos_move.append(x_stop * 10 + y_stop)
    elif (x_stop == x_start - 2) and (y_stop == y_start - 1):
        pos_move.append(x_stop * 10 + y_stop)

    return pos_move
    #  testat ok


def knight_can_move(pos_start, chess_table_current, number_move):
    #  returneaza adevarat daca un cal existent pe tabla mai poate muta
    #  in cazul in care regele nu poate muta, pt verificare remiza (pat)
    x_start = chess_pos_dictionary[pos_start] // 10
    y_start = chess_pos_dictionary[pos_start] % 10

    pos_moves = []

    if (x_start > 2) and (y_start > 2) and (x_start < 7) and (y_start < 7):
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 1) and (y_start == 1):
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
    elif (x_start == 8) and (y_start == 8):
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 1) and (y_start == 8):
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
    elif (x_start == 8) and (y_start == 1):
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
    elif (x_start > 2) and (y_start == 1) and (x_start < 7):
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
    elif (x_start > 2) and (y_start == 8) and (x_start < 7):
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 1) and (y_start > 2) and (y_start < 7):
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
    elif (x_start == 8) and (y_start > 2) and (y_start < 7):
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 2) and (y_start == 2):
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
    elif (x_start == 7) and (y_start == 2):
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
    elif (x_start == 2) and (y_start == 7):
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 7) and (y_start == 7):
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 2) and (y_start == 1):
        pos_moves.append((x_start + 2) * 10 + (y_start + 1))
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
    elif (x_start == 2) and (y_start == 8):
        pos_moves.append((x_start + 2) * 10 + (y_start - 1))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
    elif (x_start == 7) and (y_start == 1):
        pos_moves.append((x_start - 2) * 10 + (y_start + 1))
        pos_moves.append((x_start - 1) * 10 + (y_start + 2))
        pos_moves.append((x_start + 1) * 10 + (y_start + 2))
    elif (x_start == 7) and (y_start == 8):
        pos_moves.append((x_start - 2) * 10 + (y_start - 1))
        pos_moves.append((x_start - 1) * 10 + (y_start - 2))
        pos_moves.append((x_start + 1) * 10 + (y_start - 2))

    moves = 0

    if number_move % 2 != 0:
        color_player = 'W_'
    else:
        color_player = 'B_'

    for i in range(1, len(pos_moves)):
        value_update = pos_moves[i - 1]
        x = value_update // 10
        y = value_update % 10
        pos_update = list_letters[x - 1] + str(list_numbers[y - 1])
        if (color_player not in chess_table_current[pos_update]) and (
                'King' not in chess_table_current[pos_update]):
            moves += 1

    if moves > 0:
        return True
    else:
        return False
    #  testat ok


def knight_move(pos_start, pos_stop, chess_table_current, number_move):
    # muta calul daca mutarea este posibila
    x_stop = chess_pos_dictionary[pos_stop] // 10
    y_stop = chess_pos_dictionary[pos_stop] % 10

    check_trace = knight_behavior(pos_start, pos_stop)

    if number_move % 2 != 0:
        color_player = 'W_'
    else:
        color_player = 'B_'

    if (x_stop * 10 + y_stop) in check_trace:
        if (color_player not in chess_table_current[pos_stop]) and (
                'King' not in chess_table_current[pos_stop]):
            value_update = chess_table_current[pos_start]
            chess_table_current[pos_stop] = value_update
            chess_table_current[pos_start] = ' '
            return chess_table_current
        else:
            return False
    else:
        return False
    #  testat ok


def knight_piece_defender(pos_stop, chess_table_current, number_move):
    #  analizeaza daca calul ataca piesa curenta,
    #  in cazul in care regele advers vrea sa mute si sa ia piesa
    if number_move % 2 != 0:
        color_player = 'W_'
    else:
        color_player = 'B_'

    trace_to_piece = []

    for i in chess_table_current.keys():
        if ('Knight1' in chess_table_current[i]) and (color_player not in chess_table_current[i]):
            trace_to_piece = knight_behavior(i, pos_stop)

    counter = 0

    if color_player not in chess_table_current[pos_stop]:
        for i in range(len(trace_to_piece)):
            value_update = trace_to_piece[i]
            key = list_letters[(value_update // 10) - 1] + str(list_numbers[(value_update % 10) - 1])
            if value_update == chess_pos_dictionary[pos_stop]:
                counter = 1
            elif ' ' in chess_table_current[key]:
                counter += 1

    if (counter >= 1) or (counter == len(trace_to_piece) - 1):
        return True
    else:
        return False
    #  testat ok
