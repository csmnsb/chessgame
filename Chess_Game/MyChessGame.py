
import Game_over as go
import Piece_decision as pd
import Move_Check as mc
import Chess_Table_Game as ctg
import Chess_Table_Initialize as cti
import King_chess as kc

#  lista cu literele de pe tabla de sah
list_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

#  lista cu numerele de pe tabla de sah
list_numbers = [1, 2, 3, 4, 5, 6, 7, 8]

#  dictionar in care se atribuie un set de valori intregi pozitilor de pe tabla de sah,
#  A este considerata 1 pe axa x, H este considerata 8 pe axa x, iar y ia valorile intre 1 si 8
#  pentru a studia matematic comportamentul fiecarei piese, a verifica mutarea posibila si a muta.
chess_pos_dictionary = {
    'A1': 11, 'B1': 21, 'C1': 31, 'D1': 41, 'E1': 51, 'F1': 61, 'G1': 71, 'H1': 81,
    'A2': 12, 'B2': 22, 'C2': 32, 'D2': 42, 'E2': 52, 'F2': 62, 'G2': 72, 'H2': 82,
    'A3': 13, 'B3': 23, 'C3': 33, 'D3': 43, 'E3': 53, 'F3': 63, 'G3': 73, 'H3': 83,
    'A4': 14, 'B4': 24, 'C4': 34, 'D4': 44, 'E4': 54, 'F4': 64, 'G4': 74, 'H4': 84,
    'A5': 15, 'B5': 25, 'C5': 35, 'D5': 45, 'E5': 55, 'F5': 65, 'G5': 75, 'H5': 85,
    'A6': 16, 'B6': 26, 'C6': 36, 'D6': 46, 'E6': 56, 'F6': 66, 'G6': 76, 'H6': 86,
    'A7': 17, 'B7': 27, 'C7': 37, 'D7': 47, 'E7': 57, 'F7': 67, 'G7': 77, 'H7': 87,
    'A8': 18, 'B8': 28, 'C8': 38, 'D8': 48, 'E8': 58, 'F8': 68, 'G8': 78, 'H8': 88,
}

#  dictionar care reprezinta tabla de sah initiala,
#  nu va suferi modificari in timpul jocului.
chess_table_init = {
    'A1': 'W_Rock1', 'B1': 'W_Knight1', 'C1': 'W_Bishop1', 'D1': 'W_Queen', 'E1': 'W_King', 'F1': 'W_Bishop2',
    'G1': 'W_Knight2', 'H1': 'W_Rock2',
    'A2': 'W_Pawn1', 'B2': 'W_Pawn2', 'C2': 'W_Pawn3', 'D2': 'W_Pawn4', 'E2': 'W_Pawn5', 'F2': 'W_Pawn6',
    'G2': 'W_Pawn7', 'H2': 'W_Pawn8',
    'A3': ' ', 'B3': ' ', 'C3': ' ', 'D3': ' ', 'E3': ' ', 'F3': ' ', 'G3': ' ', 'H3': ' ',
    'A4': ' ', 'B4': ' ', 'C4': ' ', 'D4': ' ', 'E4': ' ', 'F4': ' ', 'G4': ' ', 'H4': ' ',
    'A5': ' ', 'B5': ' ', 'C5': ' ', 'D5': ' ', 'E5': ' ', 'F5': ' ', 'G5': ' ', 'H5': ' ',
    'A6': ' ', 'B6': ' ', 'C6': ' ', 'D6': ' ', 'E6': ' ', 'F6': ' ', 'G6': ' ', 'H6': ' ',
    'A7': 'B_Pawn8', 'B7': 'B_Pawn7', 'C7': 'B_Pawn6', 'D7': 'B_Pawn5', 'E7': 'B_Pawn4', 'F7': 'B_Pawn3',
    'G7': 'B_Pawn2', 'H7': 'B_Pawn1',
    'A8': 'B_Rock2', 'B8': 'B_Knight2', 'C8': 'B_Bishop2', 'D8': 'B_Queen', 'E8': 'B_King', 'F8': 'B_Bishop1',
    'G8': 'B_Knight1', 'H8': 'B_Rock1',
}

# initializare pozitii de start, stop, coordonate x, y pentru start si pentru stop
pos_start = ''
pos_stop = ''
x_start = 0
y_start = 0
x_stop = 0
y_stop = 0

#  se tipareste mesajul de inceput, se alege numele jucatorilor
#  jucatorul numarul 1 va fi considerat ca are piesele abe, cel cu numarul 2, piesele negre.
print()
print(f"Bine ați venit la jocul de șah!\n")
print(f"Introduceți numele jucătorilor.\n")
player1 = input("Alege numele jucătorului cu piesele albe: ")
print()
player2 = input("Alege numele jucătorului cu piesele negre: ")
print(f"\n")

#  se printeaza tabla de sah de inceput de joc
cti.chess_table_initialize(chess_table_init)

#  initializare dictionar reprezentand tabla de sah pentru mutari,
#  care va retine tabla de sah in timpul jocului, se va actualiza constant.
chess_table_current = {}
for key in chess_table_init.keys():
    value = chess_table_init[key]
    chess_table_current.update({key: value})

print(f"Introduceți mutarea în formatul 'LcLc' (L = Literă, c = cifră):")

# initializare jucator curent, este jucatorul cu numarul 1, cel cu piesele albe
current_player = player1

#  initializare numar de mutari efectuate, jucatorul cu numarul 1 efectueaza prima mutare
#  fiind cu piesele albe
#  jucatorul cu piesele albe executa mutarile cu numar impar, cel cu piesele negre, mutarile cu numar par
number_move = 1
moves_in_game = ""
print()
print(f"Mutare {current_player.title()} cu piesele albe(W_):")
move_player = input("> ")
print()

while True:
    if (move_player == 'quit') or (move_player == 'exit'):
        print(f"Mulțumim pentru joc {player1.title()} și {player2.title()}!")
        break

    #  se verifica daca mutarea introdusa este pe tabla de sah
    #  daca este atunci se creaza pozitia de start si pozitia de stop pentru cheile din dictionar
    #  si se stabilesc pozitiile lui x si y de start si de stop
    moves_in_game += str(number_move) + "." + move_player.upper() + " "
    if mc.move_check(move_player):
        pos_start = move_player[0].title() + move_player[1]
        pos_stop = move_player[2].title() + move_player[3]
        x_start = chess_pos_dictionary[pos_start] // 10
        y_start = chess_pos_dictionary[pos_start] % 10
        x_stop = chess_pos_dictionary[pos_stop] // 10
        y_stop = chess_pos_dictionary[pos_stop] % 10

        # se decide culoarea jucatorului care se afla la mutare
        if number_move % 2 != 0:
            color_player = 'W_'
        else:
            color_player = 'B_'

        pos_king = ''

        # se afla pozitia curenta a regelui jucatorului care se afla la mutare
        for i in chess_table_current.keys():
            if ('King' in chess_table_current[i]) and (color_player in chess_table_current[i]):
                pos_king = i

        # daca regele este in sah, nu se permite mutarea altei piese
        if kc.king_chess_current(pos_king, chess_table_current, number_move):
            print(f"Regele este in sah, trebuie sa mutati regele\n")
        # daca regele nu este in sah, se permite mutarea aleasa
        # se decide ce piesa se afla la pozitia de start pentru a studia comportamentul ei
        # si daca mutarea este posibila o efectueaza si apoi incrementeaza numarul de mutari efectuate
        elif pd.piece_decision(pos_start, pos_stop, chess_table_current, number_move, moves_in_game):
            ctg.chess_table_game(chess_table_current)
            # se verifica daca jocul s-a terminat
            if go.game_over(chess_table_current, number_move):
                break
            number_move += 1

    #  se cere introducerea de la consola a mutarii umratoare
    #  daca jocul nu s-a terminat

    if number_move % 2 != 0:
        current_player = player1
        print()
        print(f"Mutare {current_player.title()} cu piesele albe( W_):")
    else:
        current_player = player2
        print()
        print(f"Mutare {current_player.title()} cu piesele negre( B_):")

    move_player = input("> ")
    print()

# print(moves_in_game)
